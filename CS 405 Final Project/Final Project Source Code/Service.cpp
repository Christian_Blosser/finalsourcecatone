/*
 * Service.cpp
 *
 *  Created on: Nov 5, 2017
 *      Author: Curtis
 */

#include "Service.h"

using namespace std;

void Service::createService()
{
	cout<<"\nPlease Enter The Service Number ";
	cin>>customer_number;
	cout<<"\nPlease Enter The Name of The Service ";
	cin>>name;
	cout<<"\nPlease Enter The Price of The Service ";
	cin>>serviceVar;
	cout<<"\nPlease Enter The Discount (%) ";
	cin>>discount;
 }

void Service::showService()
{
	 cout<<"\nThe Service Number : "<<customer_number;
	 cout<<"\nThe Name of The Service : ";
	 puts(name);
	 cout<<"\nThe Price of The Service : "<<serviceVar;
	 cout<<"\nDiscount : "<<discount;
 }

// NOT NEEDED FOR TRANSLATION
//void Service::storeService(void)
//{
//	service_number = new int;		//memory leak
//	service_buff = (char *)malloc(10);
//	number_array = new int[10];
//
//	service_buff = service_buff;		//changed == to = @suppress("Assignment to itself")
//	service_number = service_number;	//changed == to = @suppress("Assignment to itself")
//	number_array = number_array;		//changed == to = @suppress("Assignment to itself")
//}
// NOT NEEDED FOR TRANSLATION
//void Service::returnServiceNumber(void)
//{
//	cout << INT_MAX+1;					//returns value larger than memory allocates, deleted +1
//}


Service::Service() {					//initialized all variables (8 total)
	number_array = {};
	service_number = 0;
	discount = 0;
	tax = 0;
	qty = 0;
	serviceVar = 0;
	service_buff = {};
	customer_number = 0;

}

Service::~Service() {
	// TODO Auto-generated destructor stub
}

